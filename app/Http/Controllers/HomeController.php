<?php

namespace App\Http\Controllers;

use App\User;
use App\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $uploadedFiles = auth()->user()->filesGrantedAccessToMe()->get();

        // Take all files that belong to other users
        // Show maximum 5 files per page
        // Show the pagination link
        $uploadedFiles = UploadedFile::where('uploaded_by', '!=', auth()->user()->id)
            ->paginate(5);

        return view('shared_files.index', [
            'uploadedFiles' => $uploadedFiles
        ]);
    }

    /**
     * Show the My files page. Lists all files that belong to the loged in user
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function myFiles()
    {
        // Take all files that belong a loged in user
        // Show maximum 5 files per page
        // Show the pagination link
        $uploadedFiles = UploadedFile::where('uploaded_by', auth()->user()->id)
            ->paginate(5);

        return view('shared_files.my_files', [
            'uploadedFiles' => $uploadedFiles
        ]);
    }

    /**
     * Show the form for uploading new file.
     *
     * @return \Illuminate\Http\Response
     */
    public function fileUploadForm()
    {
        return view('shared_files.upload_file');
    }

    /**
     * Store a newly created file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeFile(Request $request)
    {
        $validatedData = $request->validate([
            // 'file' => 'required|mimes:|max:10485',
            'file' => [
                'required',
                'file',
                'max:10485', 
            ],
        ]);
        
        // if a file wit less size of 10MB passes check extensions
        // if the file is with the exe, bmp or php extension, redirect back with the error message
        $extension = $request->file->getClientOriginalExtension();

        if (
            // $request->file->getMimeType() == "text/php" || 
            // $request->file->getMimeType() == "text/x-php" || 
            // $request->file->getMimeType() == "application/x-dosexec" || 
            // $request->file->getMimeType() == "application/octet-stream" ||
            // $request->file->getMimeType() == "image/bmp"  ||
            $extension == "exe" ||
            $extension == "bmp" ||
            $extension == "php"
        ) {

            $validator = Validator::make([], [], []);
            $validator->getMessageBag()->add('error', __('The .exe, .bmp, *.php extensions are not allowed!'));
            // redirect back with inputs and validator instance
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $uploadedFile = $validatedData['file'];
        $orginalFileName = $uploadedFile->getClientOriginalName();

        $fileName = md5(time()) . '.' . $uploadedFile->extension();

        if ($request->file->storeAs('uploads', $fileName)) {

            $UploadedFile = new UploadedFile();
            $UploadedFile->orginal_file_name = $orginalFileName;
            $UploadedFile->stored_file_name = $fileName;
            $UploadedFile->public_link = substr(md5(time()), 0, 7);
            $UploadedFile->uploaded_by = auth()->user()->id;
            $UploadedFile->save();

            return redirect()->route('file.show', $UploadedFile->public_link);
        }

        return back()->with('error', __('Something wrong happen'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UploadedFile  $UploadedFile
     * @return \Illuminate\Http\Response
     */
    public function showFileInfo($uploadedFileHash)
    {
        //I have made the option for granting permission to the file 
        //$users = User::where('id', '!=', auth()->user()->id)->orderBy('name')->get();

        $uploadedFile = UploadedFile::where('public_link', $uploadedFileHash)->first();

        return view('shared_files.file_info', [
            'uploadedFile' => $uploadedFile,
            //'users' => $users,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UploadedFile  $uploadedFile
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request, UploadedFile $uploadedFile)
    {
        // Here I could implement a logic for the permissions as well

        $orginalFileName = $uploadedFile->orginal_file_name;
        $storedFileNamePath = 'uploads/' . $uploadedFile->stored_file_name;

        if (Storage::exists($storedFileNamePath)) {
            $uploadedFile->download_counter += 1;
            $uploadedFile->save();
        }

        return Storage::download($storedFileNamePath, $orginalFileName);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UploadedFile  $uploadedFile
     * @return \Illuminate\Http\Response
     */
    public function controlUsersAccess(Request $request, UploadedFile $uploadedFile)
    {
        if ($uploadedFile->uploaded_by != auth()->user()->id) {
            abort(401);
        }

        $validatedData = request()->validate([
            'granted_users' => ['nullable', 'array']
        ]);

        $grantedUsers = $validatedData['granted_users'] ?? [];

        $uploadedFile->users()->sync($grantedUsers);

        return back()->with('success', __('The action was successful!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UploadedFile  $uploadedFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(UploadedFile $uploadedFile)
    {
        if ($uploadedFile->uploaded_by != auth()->user()->id) {
            abort(401);
        }

        Storage::delete('uploads/' . $uploadedFile->stored_file_name);
        if ($uploadedFile->delete()) {
            return back()->with('success', __('The file was deleted successfuly!'));
        } else {
            return back()->with('error', __('The file cannot be deleted!'));
        }
    }
}
