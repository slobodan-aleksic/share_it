<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class UploadedFile extends Model
{
    protected $casts = [
        'is_sharable' => 'boolean'
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'shared_files', 'uploaded_file_id', 'shared_with_user_id')
            ->withTimestamps();
    }
}
