<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSharedFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_files', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('uploaded_file_id');
            $table->foreign('uploaded_file_id')->references('id')->on('uploaded_files')->onDelete('cascade');
            $table->index(['uploaded_file_id']);

            $table->unsignedBigInteger('shared_with_user_id');
            $table->foreign('shared_with_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->index(['shared_with_user_id']);

            $table->unique(['uploaded_file_id', 'shared_with_user_id'], 'un_shared_files');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_files');
    }
}
