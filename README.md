
# SHARE IT

## The app is made for the test purpose!

- User can register and log in to the portal
- User can see the uploaded files list by another users if any - pagination is added 5 files per page
- User can see see the uploaded files list by him when pressing the button My files - pagination is added 5 files per page
- User can update all files except files with the BMP, EXE, PHP extensions with maximum size of 10MB
- User can share the file link to anyone - it is public
- Everybody who has the link can reshere it or download a related file


## Added functionality but commented out
- User can grant access to chosen users from multiselect list so that chosen users can only see shared by grantee

## One simple test is writen