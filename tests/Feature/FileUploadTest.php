<?php

namespace Tests\Feature;

use App\UploadedFile;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile as UF;
use Illuminate\Support\Facades\Storage;

use Tests\TestCase;

class FileUploadTest extends TestCase
{
    use RefreshDatabase, WithFaker;


    protected $user1;
    protected $user2;


    protected function setUp(): void
    {
        parent::setUp();

        $this->user1 = factory(User::class)->create([
            'email' => 'user1@share.it'
        ]);

        $this->user2 = factory(User::class)->create([
            'email' => 'user1@share.it'
        ]);
    }

    /**
     * @test
     */
    public function test_user_can_upload_file()
    {
        $this->actingAs($this->user1);

        Storage::fake('campaign');
        $sizeInKilobytes = 100;

        $file = UF::fake()->create('document.csv', $sizeInKilobytes);

        $response = $this->json('POST', 'file-upload', [
            'file' => $file,
        ]);

        $uploadedFile = UploadedFile::latest()->first();

        $this->assertEquals('uploads/' . $file->hashName(), 'uploads/' . $uploadedFile->stored_file_name);

        Storage::disk()->assertExists('uploads/' . $file->hashName());

        $response->assertOk();
    }

    /**
     * @test
     */
    public function test_user_can_access_the_link()
    {
        $this->actingAs($this->user1);

        Storage::fake('campaign');
        $sizeInKilobytes = 100;

        $file = UF::fake()->create('document.csv', $sizeInKilobytes);

        $response = $this->json('POST', 'file-upload', [
            'file' => $file,
        ]);

        $uploadedFile = UploadedFile::latest()->first();

        $response = $this->get(route('file.download', $uploadedFile));

        $response->assertStatus(200);

        $response->assertOk();
    }

    /**
     * @test
     */
    public function test_user_cannot_upload_file_if_bmp()
    {
        $this->defaultDataForFormat('image.bmp');
    }

    /**
     * @test
     */
    public function test_user_cannot_upload_file_if_exe()
    {
        $this->defaultDataForFormat('program.exe');
    }

    /**
     * @test
     */
    public function test_user_cannot_upload_file_if_php()
    {
        $this->defaultDataForFormat('script.php');
    }

    /**
     * @test
     */
    public function defaultDataForFormat($fileN)
    {
        $this->actingAs($this->user1);

        Storage::fake('campaign');
        $sizeInKilobytes = 100;

        $file = UF::fake()->create($fileN, $sizeInKilobytes);

        $response = $this->json('POST', 'file-upload', [
            'file' => $file,
        ]);

        $response->assertSessionHasErrors('error');
    }
}
