<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['web', 'auth'])
    ->group(function() {
        Route::get('home', 'HomeController@index')->name('home');

        Route::get('my-files', 'HomeController@myFiles')->name('files.list.my');
        
        Route::get('file-upload-form', 'HomeController@fileUploadForm')->name('file.upload.form');
        Route::post('file-upload', 'HomeController@storeFile')->name('file.upload.post');

        // Route::get('{uploadedFileHash}', 'HomeController@showFileInfo')->name('file.show');
        // Route::get('download-file/{uploadedFile}', 'HomeController@download')->name('file.download');

        Route::get('delete-file/{uploadedFile}', 'HomeController@destroy')->name('file.destroy');

       

        //Route::put('control-users-access/{uploadedFile}', 'HomeController@controlUsersAccess')->name('file.controlUsersAccess');
    });

    Route::get('{uploadedFileHash}', 'HomeController@showFileInfo')->name('file.show');
    Route::get('download-file/{uploadedFile}', 'HomeController@download')->name('file.download');