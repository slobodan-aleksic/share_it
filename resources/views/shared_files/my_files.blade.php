@extends('layouts.app')

@section('content')
<div class="container">

    @include('shared_files.messages')

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            {{ __('My Files') }}     
                        </div>

                        <div class="col-6 text-right">
                            <a class="btn btn-secondary" href="{{ route('home') }}">
                                {{ __('Back') }}
                            </a>        
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Action') }}
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <a class="btn btn-primary" href="{{ route('file.upload.form') }}">
                                {{ __('Upload file') }}
                            </a>        
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Uploaded files') }}
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

            
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-uppercase">
                                    {{ __('File Name') }}
                                </th>
                                <th class="text-uppercase">
                                    {{ __('Public link') }}
                                </th>
                                <th class="text-uppercase">
                                    {{ __('Time uploaded') }}
                                </th>
                                <th class="text-uppercase">
                                    {{ __('Number of downloads') }}
                                </th>
                                <th class="text-uppercase">
                                    {{ __('Action') }}
                                </th>
                            </tr>
                        </thead>
                        <thead>
                            @foreach($uploadedFiles as $uploadedFile)
                            <tr>
                                <td class="text-bold">
                                    <a href="{{ route('file.show', $uploadedFile->public_link) }}">
                                    {{ $uploadedFile->orginal_file_name }}
                                    </a>
                                </td>
                                <td class="text-uppercase text-center">
                                    <div class="input-group">
                                        <input class="form-control" type="text" value="{{ route('file.show', $uploadedFile->public_link) }}" id="myInput_{{$uploadedFile->id}}">
                                        <button 
                                            onclick="myFunction_{{$uploadedFile->id}}()" 
                                            class="btn btn-primary input-group-append">{{ __('CLP') }}
                                        </button>
                                        <script>
                                        function myFunction_{{$uploadedFile->id}}() {
                                            var copyText = document.getElementById("myInput_{{$uploadedFile->id}}");
                                            copyText.select();
                                            copyText.setSelectionRange(0, 99999);
                                            document.execCommand("copy");
                                        }
                                        </script>
                                    </div>
                                </td>
                                <td class="text-uppercase text-center">
                                    {{ $uploadedFile->created_at->format('Y-m-d H:I') }}
                                </td>
                                <td class="text-uppercase text-center">
                                    {{ $uploadedFile->download_counter }}
                                </td>
                                <td class="text-uppercase">
                                    <form action="{{ route('file.destroy', $uploadedFile) }}">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn btn-danger btn-sm">
                                            {{ __('Delete') }}
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </thead>
                    </table>

                </div>

                <div class="card-footer">
                    {{ $uploadedFiles->links() }}
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
