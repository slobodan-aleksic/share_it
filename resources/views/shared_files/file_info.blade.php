@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @include('shared_files.messages')

    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                
                        <div class="col-6">
                            {{ __('File info') }}   
                        </div>
                        <div class="col-6 text-right">
                            @guest 
                            @else
                            <a class="btn btn-secondary" href="{{  route('files.list.my') }}">
                                {{ __('Back') }}
                            </a>   
                            @endguest     
                        </div>
                        
                    </div>
                </div>

                <div class="card-body">

                    @if ($uploadedFile)

                        <table class="table">
                            <thead>
                                <tr>
                                    <td class="text-uppercase">
                                        <b>
                                            {{ __('File Name') }}
                                        </b>
                                    </td>
                                    <td class="text-uppercase">
                                        {{ $uploadedFile->orginal_file_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-uppercase">
                                        <b>
                                            {{ __('Time uploaded') }}
                                        </b>
                                    </td>
                                    <td class="text-uppercase">
                                        {{ $uploadedFile->created_at->format('Y-m-d H:I') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-uppercase">
                                        <b>
                                            {{ __('Public link') }}
                                        </b>
                                    </td>
                                    <td class="text-uppercase">
                                        <div class="input-group">
                                            <input class="form-control" type="text" value="{{ route('file.show', $uploadedFile->public_link) }}" id="myInput_{{$uploadedFile->id}}">
                                            <button 
                                                onclick="myFunction_{{$uploadedFile->id}}()" 
                                                class="btn btn-primary input-group-append">{{ __('Copy to Clipboar') }}
                                            </button>
                                            <script>
                                            function myFunction_{{$uploadedFile->id}}() {
                                                var copyText = document.getElementById("myInput_{{$uploadedFile->id}}");
                                                copyText.select();
                                                copyText.setSelectionRange(0, 99999);
                                                document.execCommand("copy");
                                            }
                                            </script>
                                        </div>
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <td class="text-uppercase">
                                        <b>
                                            {{ __('Download Counter') }}
                                        </b>
                                    </td>
                                    <td class="text-uppercase">
                                        {{ $uploadedFile->download_counter }}
                                    </td>
                                </tr> --}}
                                <tr>
                                    <td class="text-uppercase">
                                        <b>
                                            {{ __('Download Counter') }}
                                        </b>
                                    </td>
                                    <td class="text-uppercase">
                                        <a href="{{ route('file.download', $uploadedFile) }}" class="btn btn-primary">
                                            {{ __('Download file') }}
                                        </a>
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <td class="text-uppercase">
                                        <b>
                                            {{ __('Grant Access to users') }}
                                        </b>
                                    </td>
                                    <td class="text-uppercase">
                                        @if(
                                            //$users->count() > 0
                                            1==0
                                        )

                                            <form action="{{ route('file.controlUsersAccess', $uploadedFile) }}" method="post">
                                                @csrf
                                                @method('PUT')
                                                <div class="row">
                                                    <div class="col-12">
                                                        <select name="granted_users[]" class="select2" multiple="true">
                                                            @foreach($users as $user)
                                                            <option value="{{ $user->id }}" {{  in_array($user->id, $uploadedFile->users()->pluck('users.id')->toArray() ?? []) ? 'selected' : '' }}>{{ $user->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-12 text-left">
                                                        <button class="btn btn-primary">
                                                            {{ __("Grant Access") }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                       
                                            <script>
                                                $(function() {
                                                    $(".select2").select2();
                                                });
                                            </script>
                                        
                                        @else 
                                        {{ __('There are no users in :appName to grant access to!', ['appName' => 'Share It']) }}
                                        @endif
                                    </td>
                                </tr> --}}
                            </thead>
                        </table>


                    @else

                        {{ __('You are not authorised to access') }}

                    @endif



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
